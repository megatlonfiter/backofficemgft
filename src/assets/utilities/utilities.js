let utilities = {
    tryParseJSON: (jsonString) => {
   
        if(!jsonString){
            return null;
        }  
       
        try {
            var obj = JSON.parse(jsonString);
            if (obj && typeof obj === "object") {
                return obj;
            }
        } catch (e) {
            return jsonString;
        }
    
    
        return null;
    },

    IsJSON: (str) => {
        try {
            JSON.parse(str);
            return true;
        } catch (error) {
            return false;
        }
    }
} 

export default utilities;