import React from "react";
import Payment from "models/Payment/Payment.js";

import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
} from "reactstrap";

function Tables() {

const [lastPayments, setLastPayments] = React.useState([]);



let getLastPays = async () => {

  var myHeaders = new Headers();
  myHeaders.append("Access-Control-Allow-Origin", "*");

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };
  
  try {
    let pagos = await fetch("http://localhost:13131/api/v1/payment/getLasts?quantity=10", requestOptions);
    // console.log(pagos);
    pagos = await pagos.json(); 
    console.log(pagos);
    if(pagos.Results != []){
      console.log(pagos.Results[0].Payer);
      setLastPayments(pagos.Results.map(x => new Payment(x)));  
    }    
    console.log(`RESULTADO FETCH PAGOS: ${JSON.stringify(pagos)}`);
    console.log(`lastPayments: ${JSON.stringify(lastPayments)}`);
  } catch (error) {
    console.log(error);
  }
}



React.useEffect(() => {
  getLastPays(); 
}, [])




  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">Pagos</CardTitle>
              </CardHeader>
              <CardBody>
                <Table className="tablesorter" responsive>
                  <thead className="text-primary">
                    <tr>
                      <th>ID</th>
                      <th>Payer</th>
                      <th>Document ID</th>
                      <th>Transaction Code</th>
                      <th>Email</th>                      
                      <th>Processor</th>                      
                      <th className="text-center">Monto</th>
                      <th className="text-center"></th>                      
                    </tr>
                  </thead>
                  
                  {
                    lastPayments.map((pay, i) => {                    
                     return (
                     <tbody key={`tbody${i}`}>
                        <tr key={`tr${i}`}>
                          <td key={`id${i}`}>{pay.Id}</td>
                          <td key={`name${i}`}>{pay.Card.identification.name ? pay.Card.identification.name : null }</td>
                          <td key={`docnumber${i}`}>{pay.Card.identification.document_number ? pay.Card.identification.document_number : null}</td>
                          <td key={`transaction${i}`}>{pay.TransactionCode}</td>
                          <td key={`email${i}`}>{pay.PayerEmail}</td>
                          <td key={`gateway${i}`}>{pay.Gateway.Name}</td>                          
                          <td key={`amount${i}`} className="text-center" style={{ fontWeight: 'bold' }}>{pay.Amount}</td>
                          <td key={`tdbtn${i}`}><button key={`btn${i}`} type="button" className="btn btn-danger text-center btn-block">Refund</button></td>
                        </tr>                    
                      </tbody>
                      )
                    })
                  }


                  
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col md="12">
            <Card className="card-plain">
              <CardHeader>
                <CardTitle tag="h4">Table on Plain Background</CardTitle>
                <p className="category">Here is a subtitle for this table</p>
              </CardHeader>
              <CardBody>
                <Table className="tablesorter" responsive>
                  <thead className="text-primary">
                    <tr>
                      <th>Name</th>
                      <th>Country</th>
                      <th>City</th>
                      <th className="text-center">Salary</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Dakota Rice</td>
                      <td>Niger</td>
                      <td>Oud-Turnhout</td>
                      <td className="text-center">$36,738</td>
                    </tr>
                    <tr>
                      <td>Minerva Hooper</td>
                      <td>Curaçao</td>
                      <td>Sinaai-Waas</td>
                      <td className="text-center">$23,789</td>
                    </tr>
                    <tr>
                      <td>Sage Rodriguez</td>
                      <td>Netherlands</td>
                      <td>Baileux</td>
                      <td className="text-center">$56,142</td>
                    </tr>
                    <tr>
                      <td>Philip Chaney</td>
                      <td>Korea, South</td>
                      <td>Overland Park</td>
                      <td className="text-center">$38,735</td>
                    </tr>
                    <tr>
                      <td>Doris Greene</td>
                      <td>Malawi</td>
                      <td>Feldkirchen in Kärnten</td>
                      <td className="text-center">$63,542</td>
                    </tr>
                    <tr>
                      <td>Mason Porter</td>
                      <td>Chile</td>
                      <td>Gloucester</td>
                      <td className="text-center">$78,615</td>
                    </tr>
                    <tr>
                      <td>Jon Porter</td>
                      <td>Portugal</td>
                      <td>Gloucester</td>
                      <td className="text-center">$98,615</td>
                    </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}

export default Tables;
