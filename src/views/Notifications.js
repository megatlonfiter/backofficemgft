import React from "react";
// react plugin for creating notifications over the dashboard
import NotificationAlert from "react-notification-alert";
import Payment from "models/Payment/Payment.js";
import moment from "moment/moment";
import LoadingOverlay from "components/LoadingOverlay.js";
import ModalAction from "components/ModalAction.js";
import constants from "variables/constants.js";

// reactstrap components
import {
  Alert,
  UncontrolledAlert,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Table,  
  Col,
  CardFooter,
  CardText,
  FormGroup,
  Form,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  CardGroup,
  ButtonDropdown  
} from "reactstrap";

import { ThreeDots } from "react-loader-spinner";

function Notifications() {
  

  const [lastPayments, setLastPayments] = React.useState([]);  
  const notificationAlertRef = React.useRef(null);
  

  const handleOptionSelect = event => {
    setSelectedOption(event.target.value);
  }; 
  
  //INPUT SELECT
  const [selectedOption, setSelectedOption] = React.useState(null);

  const emailRef = React.useRef();
  const [emailForm, setEmailForm] = React.useState(null);

  const transactionCodeRef = React.useRef();
  const [transactionCodeForm, setTransactionCodeForm] = React.useState("");

  const [isLoading, setIsLoading] = React.useState(false);

  
  let blurEmail = event => {    
    setEmailForm(event.target.value);
  }



  let blurTransactionCode = event => {
    setTransactionCodeForm(event.target.value);
    console.log(transactionCodeForm);
  }

  const quantityRef = React.useRef(null);
  const [quantityForm, setQuantityForm] = React.useState(10);  
  let onSelectedQuantity = (event) => {    
    setQuantityForm(event.target.value);   
    // setQuantityForm(quantityRef.current.props.value);
    // console.log(quantityForm);
  }

  const stateIdRef = React.useRef(null);
  const [stateIdForm, setStateIdForm] = React.useState(null);  

  let onSelectedStateId = (event) => {    
    setStateIdForm(event.target.value);   
    // setQuantityForm(quantityRef.current.props.value);
    // console.log(quantityForm);
  }


  let getStateDescription = (stateId) => {    
  let stateObj = constants.paymentsStateId.filter( x => x.id == stateId);
  return stateObj.description;    
  }



  let getLastPays = async () => {

    setIsLoading(true);
    var myHeaders = new Headers();
    myHeaders.append("Access-Control-Allow-Origin", "*");

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    var params = {
      quantity: quantityForm,
      payerEmail: emailForm,
      transactionCode: transactionCodeForm
    };    

    var queryString = new URLSearchParams(params).toString();
    
    console.log(queryString);

    try {      
      let pagos = await fetch(`http://localhost:13131/api/v1/payment/getLast?${queryString}`, requestOptions);
      // console.log(pagos);
      let pagosjson = await pagos.json(); 
      console.log(pagosjson);
      
      if(pagosjson.Results){
        console.log(pagosjson.Results[0].Payer);
        setLastPayments(pagosjson.Results.map(x => new Payment(x)));  
      }
      else{ 
        setLastPayments("Sin resultados para esta búsqueda")
      }


      console.log(`RESULTADO FETCH PAGOS: ${JSON.stringify(pagosjson)}`);
      console.log(`lastPayments: ${JSON.stringify(lastPayments)}`);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  }

  React.useEffect(() => {    
    console.log(emailRef)
  }, [quantityForm, emailForm])  
  

  const notify = (place, color, info, paymentId = null, gatewayId = null) => {    
    var type;
    switch (color) {
      case 1:
        type = "primary";
        break;
      case 2:
        type = "success";
        break;
      case 3:
        type = "danger";
        break;
      case 4:
        type = "warning";
        break;
      case 5:
        type = "info";
        break;
      default:
        break;
    }


    var options = {};
    options = {
      place: place,
      message: (
        <div>
          <div>            
           {info}
            {
            paymentId != null ? <Button block color="danger" onClick={()=> refund(paymentId, gatewayId)}>Devolver Pago</Button> : null
            }
          </div>
        </div>
      ),
      type: type,
      icon: "tim-icons icon-credit-card",
      autoDismiss: 5,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  let refund = async (paymentId, gatewayId) => {

    if(!paymentId){notify("tc", 3, 'Sin ID de Pago'); return}
    if(!gatewayId){notify("tc", 3, 'Sin GatewayId del Pago'); return}

    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", "Bearer 36Htu-pKOWO7_GRcIdRO_77_RyC1Bxx8a1cwdDyBg-NR8iwHI0GrnN6dzk7pKplzde-wIyfciVVjpoVmks7zWFcl8IndTBB-Ahp8-bgHJOSQ2OIiDmKVRt1IQALGZflzWH_Aw7iH4hphiCIAW9D0AE-5fTcBbLanhqypFiaUG76crT9nLhFmLtBisbmrZaZvEdDEknOmjk9GzCade17gHFJy0xm9q8pv-gCYwyuUs4E");
    myHeaders.append("Cookie", "ARRAffinity=41ff0004ce8df3d9628feaa429dac636aae8608058c826da42492aaf5d27ed64");
    
    var raw = JSON.stringify({
      "Id": paymentId,
      "gatewayId": gatewayId
    });
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };



    try {      
      let refundPost = await fetch(`http://mgft-payments-stage.azurewebsites.net/api/v1/payment/refund`, requestOptions);
      console.log(refundPost);
      let refundjson = await refundPost.json();
      console.log(`REFUND JSON: ${JSON.stringify(refundjson)}`)
      refundjson = JSON.parse(JSON.stringify(refundjson).toLowerCase())
      
      switch (refundjson.status.code) {
        case -1:
          if(refundjson.results.refundRequest.id){
            notify("tc", 3, `El valor ${paymentId} no es válido como ID de Pago`); return;
          }
          break;
        case 500:
          if(refundjson.status.description == `payment doesn't exist`){
            notify("tc", 3, `No existe un pago para ID ${paymentId}`); return;
          }
          else{
            notify("tc", 3, refundjson.status.description);
          }
          
          
          break;
        case 0:
          switch (refundjson.results.statedetails) {
            case 'approved':
              notify("tc", 2, 'Pago Devuelto con Éxito')
              break;
            case refundjson.results.statedetails.includes('too old'):
              notify("tc", 3, `El pago es muy antiguo para ser devuelto`);
              break;                     
          }
          break;      
        default:
          notify("tc", 3, `No se pudo devolver el pago.`); 
          break;
      }
      console.log(`RESULTADO FETCH REFUND: ${JSON.stringify(refundjson)}`);
      await getLastPays();            
    } catch (error) {
      console.log(error);
      notify("tc", 3, `ERROR: ${error}`); 
    }
  }



  const handleFormSubmit = (e) => {
    e.preventDefault(); // Prevenir el comportamiento predeterminado del formulario
    getLastPays();
  };


  return (
    <>
      <div className="content">

      {isLoading == true ? 
        <LoadingOverlay/>
        :
        null
      }
      <Col md="12">
            <Card>
              <CardHeader>
                <CardTitle tag="h4">Pagos</CardTitle>
              </CardHeader>
              
              <CardGroup>
              <Col className="pl-md-1" md="2">
                      <FormGroup>
                        <label style={{ marginLeft: '2%', fontWeight: 'bold'  }}>EMAIL</label>
                        <Form onSubmit={(e) => handleFormSubmit(e)}>
                        <Input placeholder="Ingrese Email" type="text" onChange={blurEmail}/>        
                        </Form>
                      </FormGroup>
                </Col>               

                <Col className="pl-md-1" md="2">
                      <FormGroup>
                        <label style={{ marginLeft: '2%', fontWeight: 'bold'  }}>Transaction Code</label>
                        <Form onSubmit={(e) => handleFormSubmit(e)}>
                        <Input placeholder="Ingrese Referencia" type="text" onChange={blurTransactionCode}/>        
                        </Form>
                      </FormGroup>
                </Col>               

                <Col className="pl-md-1" md="1">
                      <FormGroup>
                      <label style={{ marginLeft: '2%', fontWeight: 'bold'  }}>CANTIDAD DE PAGOS</label>
                      <Input type="select" value={selectedOption} onChange={onSelectedQuantity} ref={quantityRef}>
                        <option value="10">10</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                        <option value="1000">1000</option>
                        <option value="2000">2000sf</option>
                      </Input>     
                      </FormGroup>
                </Col>  

                <Col className="pl-md-1" md="1">
                      <FormGroup>
                      <label style={{ marginLeft: '2%', fontWeight: 'bold'  }}>ESTADO DEL PAGO</label>
                      <Input type="select" value={null} onChange={onSelectedStateId} ref={stateIdRef}>
                        <option value={null}>Todos</option>
                        {                          
                          constants.paymentsStateId.map( x => {
                            return (
                              <option value={x.id}>{x.description}</option>
                            )
                          })
                        }
                      </Input>     
                      </FormGroup>
                </Col>  

                <Col className="pl-md-1" md="2">
                      <FormGroup>
                      <label style={{ marginLeft: '2%', fontWeight: 'bold' }}></label>
                      <Button block color="primary" onClick={getLastPays}>Buscar</Button>                               
                      </FormGroup>                     
                </Col> 
              </CardGroup>


              <CardBody>
                <Table className="tablesorter" responsive>
                  <thead className="text-primary">
                    <tr>
                      <th>ID</th>
                      <th>Payer</th>
                      <th>Document ID</th>
                      <th>Transaction Code</th>
                      <th>StateDetail</th>
                      <th>Email</th>                      
                      <th>Processor</th> 
                      <th>Created</th> 
                      <th className="text-center">Monto</th>
                      <th className="text-center"></th>                      
                    </tr>
                  </thead>


                  {lastPayments != 'Sin resultados para esta búsqueda' ?
                    lastPayments.map((pay, i) => {                    
                      return(
                      <tbody>
                         <tr>
                           <td>{pay.Id}</td>
                           <td>{pay.Card ? pay.Card.identification.name : null}</td>
                           <td>{pay.Card ? pay.Card.identification.document_number : null}</td>
                           <td>{pay.TransactionCode}</td>
                           <td>{getStateDescription(pay.StateId)}</td>
                           <td>{pay.PayerEmail}</td>
                           <td>{pay.Gateway.Name}</td>
                           <td>{moment(pay.Created, 'YYYY-MM-DDTHH:mm:ss.SS').format('DD-MM-YYYY HH:mm:ss')}</td>                  
                           <td className="text-center" style={{ fontWeight: 'bold' }}>${pay.Amount.toLocaleString('es-ES')}</td>
                           <td>
                           {pay.StateId == 2 ? 
                          //  <Button block color="danger" onClick={() => notify("tc", 5, `¿Está seguro de devolver a la tarjeta de ${pay.Card.identification.name} el pago ${pay.Id} por $${pay.Amount.toLocaleString('es-ES')}?`, pay.Id, pay.GatewayId)}>Refund</Button>
                          <ModalAction textButton="Refund" 
                                        title="Devolución"
                                        bodyMessage={`¿Está seguro de devolver a la tarjeta de ${pay.Card.identification.name} el pago ${pay.Id} por $${pay.Amount.toLocaleString('es-ES')}?`}
                                        onOk={()=> refund(pay.Id, pay.GatewayId)}
                                        onCancel={()=> getLastPays}                          
                          />
                           : 
                           null                          
                           }
                           </td>
                         </tr>                    
                       </tbody>
                       
                       )})
                    :
                    <tbody>
                      <tr>
                      <td colSpan="9" className="text-center">
                      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '2%' }}>
                      <label style={{ fontSize: "200%", fontWeight: 'bold'  }}>{lastPayments}</label>
                      </div>
                    </td>                  
                    </tr>                    
                    </tbody> 
                  }
                </Table>
              </CardBody>
            </Card>
          </Col>


        <div style={{zIndex: 9999}} className="react-notification-alert-container">
          <NotificationAlert ref={notificationAlertRef} />
        </div>       
      </div>
    </>
  );
}

export default Notifications;
