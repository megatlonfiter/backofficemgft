let constants = {

    paymentsStateId: [
        {
            id: 1,
            description: "Creado"        
        },
        {
            id: 2,
            description: "Aprobado"        
        },
        {
            id: 3,
            description: "Pendiente"        
        },
        {
            id: 4,
            description: "Fallido"        
        },
        {
            id: 5,
            description: "Cancelado"        
        },
        {
            id: 6,
            description: "Reintegrado"        
        },
        {
            id: 7,
            description: "Mediación"        
        },
        {
            id: 8,
            description: "Rechazado" 
        }
    ]

}


export default constants;