import Gateway from "./Gateway.js";
import InstrumentParam from "./InstrumentParam.js";
import Card from "./Card.js";
import Payer from "./Payer.js";
import utilities from "assets/utilities/utilities.js";

class Payment {

    constructor(result) {      
      this.Gateway = result.Gateway && utilities.IsJSON(result.Gateway) ? new Gateway(result.Gateway) : result.Gateway;      
      this.Payer = result.Payer && utilities.IsJSON(result.Payer)  ? new Payer(JSON.parse(result.Payer)) : null;           
      this.Card= result.Card && utilities.IsJSON(result.Card) ? new Card(JSON.parse(result.Card)) : null;     
      this.InstrumentParam = result.InstrumentParams.map(ip => ip ? new InstrumentParam(ip) : null);
      this.GatewayId = result.GatewayId;
      this.StateId = result.StateId;
      this.StateDetails = result.StateDetails;
      this.VirtualInstrumentParams = result.VirtualInstrumentParams;
      this.TransactionCode = result.TransactionCode;
      this.Amount= result.Amount;
      this.Currency= result.Currency;
      this.Description= result.Description;
      this.OnFinishJs=  result.OnFinishJs;
      this.Created= result.Created;
      this.ExtenalRef= result.ExtenalRef;
      this.Source= result.Source;
      this.PayerEmail= result.PayerEmail;      
      this.ApiVersion = result.ApiVersion;
      this.Id= result.Id;     
    }    

  }
  export default Payment;