import Identification from "./Identification.js";

class Card {

    constructor(card) {      
      this.identification = new Identification(card.identification);      
      this.expiration_month = card.email;
      this.expiration_year = card.expiration_year;
      this.first_six_digits = card.first_six_digits;
      this.last_four_digits = card.last_four_digits;
      this.payment_method = card.payment_method;
      this.issuer_id = card.issuer_id;
      this.card_token = card.card_token;


    }

  }
  export default Card;