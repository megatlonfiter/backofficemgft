class Gateway {

    constructor(gtw) {      
      this.Name = gtw.Name;
      this.TypeId = gtw.TypeId;
      this.ProcessorId = gtw.ProcessorId;
      this.Id = gtw.Id;
    }

  }
  export default Gateway;