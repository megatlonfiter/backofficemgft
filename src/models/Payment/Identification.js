class Identification {

    constructor(identification) {   
      this.document_type = identification.document_type ;
      this.document_number = identification.document_number;
      this.name = identification.name;
    }

  }
  export default Identification;