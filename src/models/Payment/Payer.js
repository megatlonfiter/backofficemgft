import Identification from "./Identification.js";

class Payer {

    constructor(p) {      
      this.identification = new Identification(p.identification);
      this.email = p.email
    }

  }
  export default Payer;


  