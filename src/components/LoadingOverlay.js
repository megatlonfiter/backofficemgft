import React from "react";
import { ThreeDots } from "react-loader-spinner";

const LoadingOverlay = () => {
  return (
    <div
      style={{
        position: "fixed",
        width: "100%",
        height: "100%",
        background: "rgba(0, 0, 0, 0.5)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 9998,
      }}
    >
      <ThreeDots height={80} width={80} color="#007bff" />
    </div>
  );
};

export default LoadingOverlay;
