import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, CardGroup } from 'reactstrap';

function ModalAction(props) {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="info" onClick={toggle}>
        {props.textButton}
      </Button>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>{props.title}</ModalHeader>
        <ModalBody>
          {props.bodyMessage}
        </ModalBody>
        <ModalFooter>
          <Button style={{margin:"2%"}} color="primary" onClick={props.onOk}>
            Sí, devolver
          </Button>{' '}
          <Button style={{margin:"2%"}} color="secondary" onClick={props.onCancel}>
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default ModalAction;